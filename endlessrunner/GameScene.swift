//
//  GameScene.swift
//  endlessrunner
//
//  Created by Jenil Shah on 3/13/17.
//  Copyright © 2017 NerdAttack. All rights reserved.
//

import SpriteKit
import GameplayKit

class GameScene: SKScene {
    var ground = SKSpriteNode()
    
    var player = SKSpriteNode(imageNamed: "player")
    
    
    
    override func didMove(to view: SKView) {
        //we will set our scene here
       self.anchorPoint = CGPoint(x: 0.5, y: 0.5)
        //print("hii123")
        createGround()
        createPlayer()
        
      
        
        
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
         //player.position = CGPoint(x: self.frame.minX + 50,y: self.frame.minY + (player.size.height / 2) + 500)
        
       
        
        // move up 20
        let jumpUpAction = SKAction.moveBy(x: 30, y:150, duration:0.5)
        // move down 20
        let jumpDownAction = SKAction.moveBy(x: 0, y:-150, duration:0.5)
        // sequence of move yup then down
        let jumpSequence = SKAction.sequence([jumpUpAction, jumpDownAction])
        
        // make player run sequence
        player.run(jumpSequence)
    }
    

    
    func createPlayer(){
        player.name = "Mario"
        player.anchorPoint = CGPoint(x: 0, y: 0.5)
        player.position = CGPoint(x: self.frame.minX + 50,y: self.frame.minY + (player.size.height / 2) + 315)
        
        self.addChild(player)
        player.alpha = 9999
    }
    
    
    override func update(_ currentTime: TimeInterval) {
        // Called before each frame is rendered
        moveGround()
    }
    
    func createGround(){           //the ground level which will be going endlessly
        for i in 0...30{
            
            let ground = SKSpriteNode(imageNamed: "mario.jpeg")
            ground.name = "Ground"
            ground.size = CGSize(width: (self.scene?.size.width)!, height: self.scene!.size.height)
            ground.anchorPoint = CGPoint(x: 0.5, y: 0.5)
            ground.position = CGPoint(x: CGFloat(i) * ground.size.width, y: (self.frame.size.height/9))
            
            self.addChild(ground)
        }
        
    }
    func moveGround(){
        self.enumerateChildNodes(withName: "Ground", using: ({
            (node,error) in
            node.position.x -= 20
            if node.position.x < -((self.scene?.size.width)!){
                node.position.x += (self.scene?.size.width)! * 3
            }
        }))
    }
    
    
}

